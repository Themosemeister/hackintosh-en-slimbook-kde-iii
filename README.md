# Instalación de Mac OS en Slimbook KDE III

Documentación y ficheros necesarios para la instalación de Mac OS en un portátil Slimbook KDE III,
proceso conocido como Hackintosh y, en este caso que la máquina cuenta con procesador AMD Ryzen,
Ryzentosh.

La documentación del proceso se encuentra en el directorio `Documentacion` y los ficheros necesarios para recrear el proceso en el directorio `OpenCore`.

# Installation of Mac OS Slimbook KDE III

Documentation and files required for the instalation of Mac OS on a Slimbook KDE III laptop, process known as Hackintosh and, in this case as the machine has an AMD Ryzen processor, Ryzentosh.

The documentation of the process can be found in the directory `Documentacion` and the files necessary to recreate the process in the directory `OpenCore`.
