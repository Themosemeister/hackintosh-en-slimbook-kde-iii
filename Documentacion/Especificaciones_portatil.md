---

title: Especificaciones del KDE Slimbook III/Slimbook ProX15-AMD
author:
date:\today

---

## Introducción

Portátil marca Slimbook, la edición KDE III que es el mismo que el ProX15 con
procesador AMD Ryzen 7, con una gráfica integrada AMD.

Es un 15" de tamaño de pantalla con un marco muy finito que hace que sea poco
mayor que un 14".

Viene con un procesador AMD Ryzen 7 4800H que integra una gráfica Radeon Vega
7 según consta en la documentación de [Wikichip][Esp00].

[Esp00]: https://en.wikichip.org/wiki/amd/ryzen_7/4800h

## Información general de la máquina

Procesador: AMD Ryzen 7 4800H
Gráfica: AMD Renoir, Radeon Wega 7.
RAM: 32GB
Disco duro: Nvme
Audio: Realtek ALC269
Tarjeta ethernet: Broadcom
Tarjeta red inalámbrica: Intel Wi-Fi AX6
Tarjeta Bluetooth: Intel AX6 200

## Procesador

- Modelo de procesador: AMD Ryzen 7 4800H with Radeon Graphics

Es un procesador con 16 núcleos y las siguientes características:

```{bash}
processor       : 15
vendor_id       : AuthenticAMD
cpu family      : 23
model           : 96
model name      : AMD Ryzen 7 4800H with Radeon Graphics
stepping        : 1
microcode       : 0x8600103
cpu MHz         : 1400.000
cache size      : 512 KB
physical id     : 0
siblings        : 16
core id         : 7
cpu cores       : 8
apicid          : 15
initial apicid  : 15
fpu             : yes
fpu_exception   : yes
cpuid level     : 16
wp              : yes
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm constant_tsc rep_good nopl nonstop_tsc cpuid extd_apicid aperfmperf pni pclmulqdq monitor ssse3 fma cx16 sse4_1 sse4_2 movbe popcnt aes xsave avx f16c rdrand lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch osvw ibs skinit wdt tce topoext perfctr_core perfctr_nb bpext perfctr_llc mwaitx cpb cat_l3 cdp_l3 hw_pstate ssbd mba ibrs ibpb stibp vmmcall fsgsbase bmi1 avx2 smep bmi2 cqm rdt_a rdseed adx smap clflushopt clwb sha_ni xsaveopt xsavec xgetbv1 xsaves cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local clzero irperf xsaveerptr rdpru wbnoinvd arat npt lbrv svm_lock nrip_save tsc_scale vmcb_clean flushbyasid decodeassists pausefilter pfthreshold avic v_vmsave_vmload vgif v_spec_ctrl umip rdpid overflow_recov succor smca
bugs            : sysret_ss_attrs spectre_v1 spectre_v2 spec_store_bypass
bogomips        : 5789.06
TLB size        : 3072 4K pages
clflush size    : 64
cache_alignment : 64
address sizes   : 48 bits physical, 48 bits virtual
power management: ts ttp tm hwpstate cpb eff_freq_ro [13] [14]
```

## Gráfica

- Modelo de gráfica: 04:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Renoir (rev c6)
  El id del PCI es 1002:1636, 1002 es del fabricante, AMD, el 1636 es el modelo.
  ```{bash}
  $ hwinfo --gfx reports
32: PCI 400.0: 0300 VGA compatible controller (VGA)
  [Created at pci.386]
  Unique ID: YmUS.q2DHP5UjuLF
  Parent ID: JZZT.K6o1zAItqiE
  SysFS ID: /devices/pci0000:00/0000:00:08.1/0000:04:00.0
  SysFS BusID: 0000:04:00.0
  Hardware Class: graphics card
  Model: "ATI Renoir"
  Vendor: pci 0x1002 "ATI Technologies Inc"
  Device: pci 0x1636 "Renoir"
  SubVendor: pci 0x1d05 "Tongfang Hongkong Limited"
  SubDevice: pci 0x109f
  Revision: 0xc6
  Driver: "amdgpu"
  Driver Modules: "amdgpu"
  Memory Range: 0xd0000000-0xdfffffff (ro,non-prefetchable)
  Memory Range: 0xe0000000-0xe01fffff (ro,non-prefetchable)
  I/O Ports: 0xe000-0xe0ff (rw)
  Memory Range: 0xfe600000-0xfe67ffff (rw,non-prefetchable)
  IRQ: 46 (no events)
  Module Alias: "pci:v00001002d00001636sv00001D05sd0000109Fbc03sc00i00"
  Driver Info #0:
    Driver Status: amdgpu is active
    Driver Activation Cmd: "modprobe amdgpu"
  Config Status: cfg=new, avail=yes, need=no, active=unknown
  Attached to: #22 (PCI bridge)

Primary display adapter: #32
```

- Modelo de chipset:

```{bash}
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 3.2.0 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
        Manufacturer: SLIMBOOK
        Product Name: PROX15-AMD
        Version: Standard
        Serial Number: Standard
        Asset Tag: Standard
        Features:
                Board is a hosting board
                Board is replaceable
        Location In Chassis: Standard
        Chassis Handle: 0x0003
        Type: Motherboard
        Contained Object Handles: 0

Handle 0x0004, DMI type 10, 6 bytes
On Board Device Information
        Type: Video
        Status: Enabled
        Description:    To Be Filled By O.E.M.

Handle 0x0028, DMI type 41, 11 bytes
Onboard Device
        Reference Designation: Onboard LAN Brodcom
        Type: Ethernet
        Status: Enabled
        Type Instance: 1
        Bus Address: 0000:02:00.0

Handle 0x0029, DMI type 41, 11 bytes
Onboard Device
        Reference Designation: HD Audio Controller
        Type: Sound
        Status: Enabled
        Type Instance: 1
        Bus Address: 0000:04:00.6
```

- Teclado, ratón y otras entradas:

```{bash}
[    0.348952] input: Power Button as /devices/LNXSYSTM:00/LNXSYBUS:00/PNP0C0C:00/input/input0
[    0.348991] input: Sleep Button as /devices/LNXSYSTM:00/LNXSYBUS:00/PNP0C0E:00/input/input1
[    0.349026] input: Lid Switch as /devices/LNXSYSTM:00/LNXSYBUS:00/PNP0C0D:00/input/input2
[    0.349061] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input3
[    0.429855] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input4
[    1.149737] input: Video Bus as /devices/LNXSYSTM:00/LNXSYBUS:00/PNP0A08:00/device:1e/LNXVIDEO:00/input/input5
[    1.457450] input: UNIW0001:00 093A:0255 Mouse as /devices/platform/AMDI0010:02/i2c-0/i2c-UNIW0001:00/0018:093A:0255.0001/input/input6
[    1.457564] input: UNIW0001:00 093A:0255 Touchpad as /devices/platform/AMDI0010:02/i2c-0/i2c-UNIW0001:00/0018:093A:0255.0001/input/input7
[    1.457639] hid-generic 0018:093A:0255.0001: input,hidraw0: I2C HID v1.00 Mouse [UNIW0001:00 093A:0255] on i2c-UNIW0001:00
[    3.415372] input: UNIW0001:00 093A:0255 Mouse as /devices/platform/AMDI0010:02/i2c-0/i2c-UNIW0001:00/0018:093A:0255.0001/input/input8
[    3.415444] input: UNIW0001:00 093A:0255 Touchpad as /devices/platform/AMDI0010:02/i2c-0/i2c-UNIW0001:00/0018:093A:0255.0001/input/input9
[    3.415500] hid-multitouch 0018:093A:0255.0001: input,hidraw0: I2C HID v1.00 Mouse [UNIW0001:00 093A:0255] on i2c-UNIW0001:00
[    3.505886] input: HD-Audio Generic HDMI/DP,pcm=3 as /devices/pci0000:00/0000:00:08.1/0000:04:00.1/sound/card0/input10
[    3.547547] snd_hda_codec_realtek hdaudioC1D0:    inputs:
[    3.557618] input: HD-Audio Generic Mic as /devices/pci0000:00/0000:00:08.1/0000:04:00.6/sound/card1/input11
[    3.557664] input: HD-Audio Generic Headphone as /devices/pci0000:00/0000:00:08.1/0000:04:00.6/sound/card1/input12
[    4.649244] input: HD Webcam: HD Webcam as /devices/pci0000:00/0000:00:08.1/0000:04:00.3/usb1/1-3/1-3:1.0/input/input13
[    4.673204] input: HD Webcam: IR Camera as /devices/pci0000:00/0000:00:08.1/0000:04:00.3/usb1/1-3/1-3:1.2/input/input14
```

- Audio:
```{bash}
**** Lista de PLAYBACK dispositivos hardware ****
tarjeta 0: Generic [HD-Audio Generic], dispositivo 3: HDMI 0 [HDMI 0]
  Subdispositivos: 1/1
  Subdispositivo #0: subdevice #0
tarjeta 1: Generic_1 [HD-Audio Generic], dispositivo 0: ALC269VC Analog [ALC269VC Analog]
  Subdispositivos: 1/1
  Subdispositivo #0: subdevice #0
```

Tenemos dos dispositivos aquí:

1. El del HDMI, que suponemos de AMD
2. Un Realtek ALC269.

- Modelos de controlador de red:

```{bash}
  *-network
       descripción: Interfaz inalámbrica
       producto: Wi-Fi 6 AX200
       fabricante: Intel Corporation
       id físico: 0
       información del bus: pci@0000:01:00.0
       nombre lógico: wlp1s0
       versión: 1a
       serie: b0:a4:60:7d:55:78
       anchura: 64 bits
       reloj: 33MHz
       capacidades: pm msi pciexpress msix bus_master cap_list ethernet physical wireless
       configuración: broadcast=yes driver=iwlwifi driverversion=5.13.0-21-lowlatency firmware=63.c04f3485.0 cc-a0-63.ucode ip=192.168.23.60 latency=0 link=yes multicast=yes wireless=IEEE 802.11
       recursos: irq:77 memoria:fea00000-fea03fff
  *-network
       descripción: Ethernet interface
       producto: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
       fabricante: Realtek Semiconductor Co., Ltd.
       id físico: 0
       información del bus: pci@0000:02:00.0
       nombre lógico: eno1
       versión: 15
       serie: b0:25:aa:45:f8:c1
       capacidad: 1Gbit/s
       anchura: 64 bits
       reloj: 33MHz
       capacidades: pm msi pciexpress msix bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt-fd autonegotiation
       configuración: autonegotiation=on broadcast=yes driver=r8169 driverversion=5.13.0-21-lowlatency firmware=rtl8168h-2_0.0.2 02/26/15 latency=0 link=no multicast=yes port=twisted pair
       recursos: irq:24 ioport:f000(size=256) memoria:fe904000-fe904fff memoria:fe900000-fe903fff
```

- Modelo de disco:

```{bash}
  *-storage
       descripción: Non-Volatile memory controller
       producto: Sandisk Corp
       fabricante: Sandisk Corp
       id físico: 0
       información del bus: pci@0000:03:00.0
       versión: 01
       anchura: 64 bits
       reloj: 33MHz
       capacidades: storage pm msi msix pciexpress nvm_express bus_master cap_list
       configuración: driver=nvme latency=0
       recursos: irq:32 memoria:fe800000-fe803fff memoria:fe804000-fe8040ff
     *-nvme0
          descripción: NVMe device
          producto: WDC WDS100T2B0C-00PXH0
          id físico: 0
          nombre lógico: /dev/nvme0
          versión: 233010WD
          serie: 21388U449210
          configuración: nqn=nqn.2018-01.com.wdc:nguid:E8238FA6BF53-0001-001B444A49B8B197 state=live
        *-namespace
             descripción: NVMe namespace
             id físico: 1
             nombre lógico: /dev/nvme0n1
             tamaño: 931GiB (1TB)
             capacidades: gpt-1.00 partitioned partitioned:gpt
             configuración: guid=a74ca438-7a69-794b-b97f-27901b3345c8 logicalsectorsize=512 sectorsize=512
  *-sata:0
       descripción: SATA controller
       producto: FCH SATA Controller [AHCI mode]
       fabricante: Advanced Micro Devices, Inc. [AMD]
       id físico: 0
       información del bus: pci@0000:05:00.0
       versión: 81
       anchura: 32 bits
       reloj: 33MHz
       capacidades: sata pm pciexpress msi ahci_1.0 bus_master cap_list
       configuración: driver=ahci latency=0
       recursos: irq:35 memoria:fe701000-fe7017ff
  *-sata:1
       descripción: SATA controller
       producto: FCH SATA Controller [AHCI mode]
       fabricante: Advanced Micro Devices, Inc. [AMD]
       id físico: 0.1
       información del bus: pci@0000:05:00.1
       versión: 81
       anchura: 32 bits
       reloj: 33MHz
       capacidades: sata pm pciexpress msi ahci_1.0 bus_master cap_list
       configuración: driver=ahci latency=0
       recursos: irq:46 memoria:fe700000-fe7007ff
```

Otra información de la máquina:

```{bash}
$ sudo lshw -short
ruta H/W              Dispositivo     Clase          Descripción
=================================================================
                                      system         PROX15-AMD (0001)
/0                                    bus            PROX15-AMD
/0/0                                  memory         64KiB BIOS
/0/a                                  memory         32GiB Memoria de sistema
/0/a/0                                memory         16GiB SODIMM DDR4 Síncrono Unbuffered (Unregistered) 3200 MHz
/0/a/1                                memory         16GiB SODIMM DDR4 Síncrono Unbuffered (Unregistered) 3200 MHz
/0/c                                  memory         512KiB L1 caché
/0/d                                  memory         4MiB L2 caché
/0/e                                  memory         8MiB L3 caché
/0/f                                  processor      AMD Ryzen 7 4800H with Radeon Graphics
/0/100                                bridge         Renoir/Cezanne Root Complex
/0/100/0.2                            generic        Renoir/Cezanne IOMMU
/0/100/1.2                            bridge         Renoir/Cezanne PCIe GPP Bridge
/0/100/1.2/0          wlp1s0          network        Wi-Fi 6 AX200
/0/100/1.3                            bridge         Renoir/Cezanne PCIe GPP Bridge
/0/100/1.3/0          eno1            network        RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
/0/100/2.4                            bridge         Renoir/Cezanne PCIe GPP Bridge
/0/100/2.4/0                          storage        Sandisk Corp
/0/100/2.4/0/0        /dev/nvme0      storage        WDC WDS100T2B0C-00PXH0
/0/100/2.4/0/0/1      /dev/nvme0n1    disk           1TB NVMe namespace
/0/100/2.4/0/0/1/1                    volume         511MiB Windows FAT volumen
/0/100/2.4/0/0/1/2    /dev/nvme0n1p2  volume         149GiB EFI partition
/0/100/2.4/0/0/1/3    /dev/nvme0n1p3  volume         649GiB EFI partition
/0/100/2.4/0/0/1/4    /dev/nvme0n1p4  volume         131GiB partición EXT4
/0/100/8.1                            bridge         Renoir Internal PCIe GPP Bridge to Bus
/0/100/8.1/0                          display        Renoir
/0/100/8.1/0.1                        multimedia     Renoir Radeon High Definition Audio Controller
/0/100/8.1/0.2                        generic        Family 17h (Models 10h-1fh) Platform Security Processor
/0/100/8.1/0.3                        bus            Renoir/Cezanne USB 3.1
/0/100/8.1/0.3/0      usb1            bus            xHCI Host Controller
/0/100/8.1/0.3/0/3                    multimedia     HD Webcam
/0/100/8.1/0.3/0/4                    bus            USB2.0 Hub
/0/100/8.1/0.3/0/4/3                  generic        USB2.0-CRW
/0/100/8.1/0.3/0/4/4                  communication  AX200 Bluetooth
/0/100/8.1/0.3/1      usb2            bus            xHCI Host Controller
/0/100/8.1/0.4                        bus            Renoir/Cezanne USB 3.1
/0/100/8.1/0.4/0      usb3            bus            xHCI Host Controller
/0/100/8.1/0.4/1      usb4            bus            xHCI Host Controller
/0/100/8.1/0.5                        multimedia     Raven/Raven2/FireFlight/Renoir Audio Processor
/0/100/8.1/0.6                        multimedia     Family 17h (Models 10h-1fh) HD Audio Controller
/0/100/8.2                            bridge         Renoir Internal PCIe GPP Bridge to Bus
/0/100/8.2/0                          storage        FCH SATA Controller [AHCI mode]
/0/100/8.2/0.1                        storage        FCH SATA Controller [AHCI mode]
/0/100/14                             bus            FCH SMBus Controller
/0/100/14.3                           bridge         FCH LPC Bridge
/0/101                                bridge         Renoir PCIe Dummy Host Bridge
/0/102                                bridge         Renoir PCIe Dummy Host Bridge
/0/103                                bridge         Renoir PCIe Dummy Host Bridge
/0/104                                bridge         Renoir Device 24: Function 0
/0/105                                bridge         Renoir Device 24: Function 1
/0/106                                bridge         Renoir Device 24: Function 2
/0/107                                bridge         Renoir Device 24: Function 3
/0/108                                bridge         Renoir Device 24: Function 4
/0/109                                bridge         Renoir Device 24: Function 5
/0/10a                                bridge         Renoir Device 24: Function 6
/0/10b                                bridge         Renoir Device 24: Function 7
/0/1                                  system         PnP device PNP0c01
/0/2                                  system         PnP device PNP0b00
/0/3                                  input          PnP device PNP0303
/0/4                                  system         PnP device PNP0c02
```
```{bash}
$ for g in `find /sys/kernel/iommu_groups/* -maxdepth 0 -type d | sort -V`; do
    echo "IOMMU Group ${g##*/}:"
    for d in $g/devices/*; do
        echo -e "\t$(lspci -nns ${d##*/})"
    done;
done;
IOMMU Group 0:
        00:01.0 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge [1022:1632]
IOMMU Group 1:
        00:01.2 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge [1022:1634]
IOMMU Group 2:
        00:01.3 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge [1022:1634]
IOMMU Group 3:
        00:02.0 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge [1022:1632]
IOMMU Group 4:
        00:02.4 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge [1022:1634]
IOMMU Group 5:
        00:08.0 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge [1022:1632]
        00:08.1 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Renoir Internal PCIe GPP Bridge to Bus [1022:1635]
        00:08.2 PCI bridge [0604]: Advanced Micro Devices, Inc. [AMD] Renoir Internal PCIe GPP Bridge to Bus [1022:1635]
        04:00.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] Renoir [1002:1636] (rev c6)
        04:00.1 Audio device [0403]: Advanced Micro Devices, Inc. [AMD/ATI] Renoir Radeon High Definition Audio Controller [1002:1637]
        04:00.2 Encryption controller [1080]: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor [1022:15df]
        04:00.3 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne USB 3.1 [1022:1639]
        04:00.4 USB controller [0c03]: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne USB 3.1 [1022:1639]
        04:00.5 Multimedia controller [0480]: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor [1022:15e2] (rev 01)
        04:00.6 Audio device [0403]: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) HD Audio Controller [1022:15e3]
        05:00.0 SATA controller [0106]: Advanced Micro Devices, Inc. [AMD] FCH SATA Controller [AHCI mode] [1022:7901] (rev 81)
        05:00.1 SATA controller [0106]: Advanced Micro Devices, Inc. [AMD] FCH SATA Controller [AHCI mode] [1022:7901] (rev 81)
IOMMU Group 6:
        00:14.0 SMBus [0c05]: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller [1022:790b] (rev 51)
        00:14.3 ISA bridge [0601]: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge [1022:790e] (rev 51)
IOMMU Group 7:
        00:18.0 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 0 [1022:1448]
        00:18.1 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 1 [1022:1449]
        00:18.2 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 2 [1022:144a]
        00:18.3 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 3 [1022:144b]
        00:18.4 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 4 [1022:144c]
        00:18.5 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 5 [1022:144d]
        00:18.6 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 6 [1022:144e]
        00:18.7 Host bridge [0600]: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 7 [1022:144f]
IOMMU Group 8:
        01:00.0 Network controller [0280]: Intel Corporation Wi-Fi 6 AX200 [8086:2723] (rev 1a)
IOMMU Group 9:
        02:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller [10ec:8168] (rev 15)
IOMMU Group 10:
        03:00.0 Non-Volatile memory controller [0108]: Sandisk Corp Device [15b7:5019] (rev 01)
```
