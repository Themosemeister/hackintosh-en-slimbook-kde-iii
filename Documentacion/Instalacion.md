% Instalación de Hackinstosh en Slimbook KDE III (ProX-15)
% Diciembre de 2021

---

title: Instalación de Mac OS (o creación de Hackintosh/Ryzentosh)
author:
date: \today

---

## Introducción

Por motivos de estudios, me veo en la obligación de instalar Mac OS para la
utilización de aplicaciones específicas para la edición de audio.

Animada por los avances en Hackintosh, voy a intentar instalarlo en esta
máquina.

## Resumen de pasos a ejecutar

Los pasos base son los siguientes:

1. Descargar sistema base Mac con macrecovery
2. Crear el pincho USB: particiones y copiar sistema Mac
3. Descargar OpenCore y empezar a preparar los ficheros:
    1. Copiar los ficheros y borrar los no imprescindibles
    2. Empezar a añadir los ficheros necesarios, importante:
        * SSDT y DSDTs personalizados (.aml) en el directorio ACPI
	* Kexts (.kext) en el directorio Kexts
	* Controladores de firmware (.efi) en el directorio drivers

## Ficheros de OpenCore

### Ficheros de firmware:
    1. HfsPlus.efi
    2. OpenRuntime.efi: ya te lo da OpenCore

### Ficheros de extensiones del núcleso (Kexts)


    1. VirtualSMC: parece que vienen muchos
        * SMCProcessor: no funciona con procesadores AMD
	* SMCSuperIO: no funciona con procesadores AMD
	* SMCLightSensor: puede causar problemas y no sabemos si este portátil 
	  tiene. Fuera.
	* SMCDellSensors: ya lo pone el nombre, para portátiles Dell
    2. Lilu
    3. WhateverGreen
    4. AppleALC:
        * AppleALCU solo soporta audio digital
	* AMD 15h/16h tienen problemas con AppleALC y no tiene soporte de 
	  micro, el Ryzen 7 4800U es el 17h, así que no tendremos problemas.
    5. Red ethernet Realtek RTL8111
    6. Tarjeta de red inalámbrica Intel Wi-fi 6 AX200: IntelItwml o algo así
    7. Bluetooth: Intel AX200: IntelBluetoothFirmware
    8. AppleMCEReporterDisables: para deshabilitar el AppleMCEReporter que
       causa pánico del núcleo.
    9. NVMeFix
    10. VoodooPS2: para teclado, ratón, etc.
    11. VoodooRMI o VoodooSMBus: tengo dudas, no lo pongo.
    12. VoodooI2C: Este creo que si.
        * He metido: VoodooI2CHID y VoodooI2CSynaptics
    13. ECEnabler
    14. BrightnessKeys

### Ficheros SSDT

    1. SSDT-EC-USBX-Desktop: específico de AMD Zen
    2. Parece, no estoy seguro, que hay que proporcionar la tabla actual del
       portátil y que se puede obtener con la utilidad SSDTTime.
    3. ¿Habrá que poner el SSDT-EC-USBX-LAPTOP? La indicación dice para 
       Skylake y más modernos, pero, ¿eso incluye un AMD?
    4. CPU Power management: con AMD nada
    5. AWAC: este bicho no necesita.
    6. Relojes del sistema en HDET:
       No tengo muy claro que estoy haciendo, voy un poco a ciegas. Aún así,
       hay que mirar el DSDT completo que se ha hecho el volcado y, a partir de
       ahí, modificar una plantilla que se descarga de SSDT.
       Después hay que compilar.
       **IMPORTANTE:**
       Si lo haces con SSDTTime, no tienes que mirar nada, te lo da solito.
    7. NVM-Fix, con SSDTTime
    8. USB RHUB, descarga
    9. IMEI, descarga
    10. SMBus: por el momento no hay utilidad para Gnu/Linux, se hace cuando
        hayamos conseguido arrancar con Mac OS, hay otra forma de sacarlo pero
	hay que utilizar el gestor de dispositivos de Windows.
	Estoy intentando sacarlo, con `lshw` tengo una especie de dirección:
	```
	/0/100/14
	```
	Pero no se exactamente como llevarlo al DSDT.
	De mirarlo mucho con `lshw`:
	```{bash}
	        *-serial
             descripción: SMBus
             producto: FCH SMBus Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 14
             información del bus: pci@0000:00:14.0
             versión: 51
             anchura: 32 bits
             reloj: 66MHz
             configuración: driver=piix4_smbus latency=0
             recursos: irq:0
         ```
	 A mi me da que es «*PC00.SMBS*» y así lo voy a hacer.
         Con Mac OS instalado habrá que comprobar que está correcto.
    11. Arreglar conflictos IRQ: no queda otra que con SDDTTime, opción 1.
    12. Renombrando la GPU, SSDT-GPU-SPOOF:
        Hay que sacar varias cosas que lo hacemos con `lspci`, y con otros.

## Configuración del `config.plist`

Con todos los ficheros en su lugar, hay que empezar con el `config.plist`, para
ello copiamos el fichero de ejemplo de OpenCore y empezaremos a modificarlo.

Para la modificación utilizaremos la utilidad ProperTree además de GenSMBios.

Lo primero es abrir ProperTree (cuidado si tu Python no está en la orden 
`python`) y hacer que lea nuestros ficheros de OpenCore mediante CTRL+CAMBIO+R.

Después siguiendo la guia de Dortania vas modificando.

En un momento determinado hay que quitar una sección completa del apartado
del núcleo, el *«path»* y copiarla desde el `patches.plist` de AMD-OSX.

Hay que modificar los parámetros referentes a los núcleos de tu procesador.

En nuestro caso:

- B8000000 0000 => B8 10 0000 0000
- BA000000 0000 => BA 10 0000 0000
- BA000000 0090 => BA 10 0000 0090

Otro detalle, hay que especificar el tipo de teclado en NVRAM, en el campo
*prev-lang:kbd*, en nuestro caso modificamos el campo a cadena y ponemos 
*es:87*.

Siguiente punto, generar un número de serie y asignarlo, para eso utilizamos
GenSMBios con la opción de MacPro7,1.

Hay que utilizar la opción 1 y después la 3.

La salida es esta:

```{bash}
  #######################################################
 #               MacPro7,1 SMBIOS Info                 #
#######################################################

Type:         MacPro7,1
Serial:       F5KZT061P7QM
Board Serial: F5K950609GUK3F7FB
SmUUID:       9F913D38-5947-4EC6-9533-60040839B658
Apple ROM:    5CF938121A25

Flushing SMBIOS entry to /home/mosel/Documentos/Hackintosh_en_Slimbook_KDE_III/OpenCore/EFI/OC/config.plist
```

De ahí se utiliza:

- Type, para Generic->SytemProductName
- Serial, para Generic->SystemSerialNumber
- Board serial, para Generic->MLB
- SmUUID, para Generic-> SystemUUID.

Después de arrancar parece que hay que arreglar la dirección MAC de la 
tarjeta de red.

Pues tras todas las modificaciones, lo único que queda es modificar ciertos 
parámetros de la BIOS, evidentemente guardar en el USB y tratar de arrancar.

