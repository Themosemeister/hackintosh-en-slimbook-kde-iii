---

title: Creación de MV con KVM/Virt Manager
author: The Mose Meister
date:\today

---

## Introducción

Partiendo del repositorio de John Colby, [macOS-KVM][Intro00], vamos a ir adaptando su configuración a
esta máquina.

[Intro00]: https://github.com/johncolby/macOS-KVM

## Adaptación de dispositivos PCI

Partimos de los originales de John y adaptamos a esta máquina.

| Original | Significado         | Slimbook KDE III|
|----------|---------------------|-----------------|
| 0b:00.3  | USB controller      |  04:00.3        |
| 0b:00.4  | Audio controller    |  04:00.6               |
| 01:00.0  | PCIe Dummy host bridge |   00:01.0    |
| 09:00.0  | VGA compatible controller|  04:00.0   |
| 09:00.1  | Audio device (HDMI) |  No tiene  |
|----------|---------------------|-----------------|


### Problemas

- Utilizar el controlador de VGA deja al portátil frito.
- El controlador USB da error.
- El controlador de Audio deja al anfitrión sin sonido y da error.

## Instalación desde [macOS-Simple-KVM][InstalaMSK00]

Punto de partida para probar MacOS en máquina virtual, el arranque, una vez
seguidas las instrucciones del repositorio funciona correctamente.

Los procesadores se emulan a Intel que parece que dan menos problemas que los AMD.

### Preparación del disco virtual en el instalador

Cuando arrancas el instalador tienes que preparar el disco (por lo visto el
proceso de instalación no lo hace de forma automática), hay que abrir el «Disk
utility», seleccionar el disco y, en un primer paso darle a «borrar» («erase»
si está en inglés), algo que no es muy intuitivo.

Tras eso ya se crea un volumen de forma automática y no se debe hacer nada más,
a no ser que se quieran crear dos particiones en el disco duro.

[InstalaMSK00]: https://github.com/foxlet/macOS-Simple-KVM
